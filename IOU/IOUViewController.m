//
//  IOUViewController.m
//  IOU
//
//  Created by Steven Taillieu on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "IOUViewController.h"
#import "IOU.h"
#import "IOUTableViewCell.h"
#import "EditViewController.h"
@interface IOUViewController ()

@property (strong, nonatomic) NSMutableArray *IOUArray;
@property (weak, nonatomic) IBOutlet UITableView *IOUTableView;
@property int IOUIndex;
@property BOOL editSelected;
@end

@implementation IOUViewController

@synthesize IOUArray;
@synthesize IOUTableView;
@synthesize IOUIndex;
@synthesize editSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [IOUTableView setDelegate:self];
    [IOUTableView setDataSource:self];
    [self popluateArray];
    editSelected = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [IOUArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IOUTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IOUTableViewCell"];
 
    if (cell == nil)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"IOUTableViewCell" owner:self options:nil];
        cell = [array lastObject];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [[cell cellPerson] setText:[(IOU *)[IOUArray objectAtIndex:[indexPath row]] person]];
        NSString *numberString;
        NSNumber *amountDue;
        amountDue = [(IOU *)[IOUArray objectAtIndex:[indexPath row]] amountDue];
        numberString = amountDue.stringValue;
        [[cell cellAmount] setText:numberString];
        double due = [amountDue doubleValue];
        
        if(due >0)
        {
            UIColor * color = [UIColor colorWithRed:113/255.0f green:175/255.0f blue:109/255.0f alpha:1.0f];

            [[cell cellAmount] setTextColor:color];
        }
        else
        {
            UIColor * color = [UIColor colorWithRed:195/255.0f green:0/255.0f blue:0/255.0f alpha:1.0f];

            [[cell cellAmount] setTextColor:color];
        }
    }
    return cell;
 }


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    editSelected = YES;
    IOUIndex = (int)[indexPath row];
   [self performSegueWithIdentifier:@"editSegue" sender:self];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if(editSelected){
        EditViewController *editViewController =  [segue destinationViewController];
        [editViewController setCurrentIOU:[IOUArray objectAtIndex:IOUIndex]];
        [editViewController setID:IOUIndex];
    }
}




//void method to create a testing IOU
-(void)popluateArray{
    IOUArray = [[NSMutableArray alloc] init];
    IOUController *controller = [[IOUController alloc] init];
    [IOUArray addObjectsFromArray:[controller retrieveIOU]];

}

@end
