//
//  IOUController.m
//  IOU
//
//  Created by Boughdan Gibbons on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "IOUController.h"

@implementation IOUController

-(void)addIOU : (IOU *) iou
{
    //create a file manager to determine if file is already present
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //create unique identifier for the dictionary keys
    NSString *UUID = [[NSUUID UUID] UUIDString];
    //this dictionary will be used to write everything to file
    NSMutableDictionary *newContent = [[NSMutableDictionary alloc] init];
    //fills dictionary with current IOU being written
    NSDictionary *setIOU = @{@"person"       : [iou person],
                             @"reasonForIOU" : [iou reasonForIOU],
                             @"amountDue"    : [iou amountDue],
                             @"date"         : [iou date],
                             @"UUID"         : UUID};
    
    if([fileManager fileExistsAtPath:[self retrieveFilePath]])
    {
        //retrieves old content from file to append to
        NSMutableDictionary *oldContent = [NSMutableDictionary dictionaryWithContentsOfFile:[self retrieveFilePath]];
        newContent = oldContent;
        //append current IOU to the base dictionary
        [newContent setObject:setIOU forKey:UUID];
        //write everything to file
        [newContent writeToFile:[self retrieveFilePath] atomically:YES];
    }
    else
    {
        //write our base dictionary before adding to it
        [newContent writeToFile:[self retrieveFilePath] atomically:YES];
        //append current IOU to base dictionary
        [newContent setObject:setIOU forKey:UUID];
        //write to file
        [newContent writeToFile:[self retrieveFilePath] atomically:YES];
    }
    NSLog(@"%@",[self retrieveFilePath]);
}

-(NSMutableArray *)retrieveIOU
{
    //retrieve contents from file
    NSDictionary *totalIOUs = [NSDictionary dictionaryWithContentsOfFile:[self retrieveFilePath]];
    //create array of keys to use as index
    NSArray *keyCount = [totalIOUs allKeys];
    //append IOU objects to this array
    NSMutableArray *addIOUObject = [[NSMutableArray alloc] init];
    //used to represent the inner dictionary
    NSDictionary *innerDic = [[NSDictionary alloc]init];
    
    for(int i = 0; i < [totalIOUs count]; i++)
    {
        //selects inner dictionary from our collection of keys
        if([totalIOUs objectForKey:[keyCount objectAtIndex:i]])
        {
            //retrieve inner dictionary
            innerDic = [totalIOUs objectForKey:[keyCount objectAtIndex:i]];
            IOU *record = [[IOU alloc]init];
            //fill iOU object
            [record setPerson:[innerDic objectForKey:@"person"]];
            [record setReasonForIOU:[innerDic objectForKey:@"reasonForIOU"]];
            [record setAmountDue:[innerDic objectForKey:@"amountDue"]];
            [record setDate:[innerDic objectForKey:@"date"]];
            [record setUUID:[innerDic objectForKey:@"UUID"]];
            //add object to our array
            [addIOUObject addObject:record];
        }
    }
    //return a nonmutable array
    //NSArray *returnIOU = addIOUObject;
    return addIOUObject;
}

-(void)updateIOU : (IOU *) iou
{
    //retrieve contents from file
    NSDictionary *totalIOUs = [NSDictionary dictionaryWithContentsOfFile:[self retrieveFilePath]];
    //create array of keys to use as index
    NSArray *keyCount = [totalIOUs allKeys];
    //append IOU objects to this array
    NSMutableDictionary *retrieveIOU = [[NSMutableDictionary alloc] init];
    //used to represent the inner dictionarys
    NSDictionary *innerDic = [[NSDictionary alloc]init];
    NSDictionary *updatedInnerDic = [[NSDictionary alloc]init];
    
    for(int i = 0; i < [totalIOUs count]; i++)
    {
        //selects inner dictionary from our collection of keys
        if([totalIOUs objectForKey:[keyCount objectAtIndex:i]])
        {
            //retrieve inner dictionary
            innerDic = [totalIOUs objectForKey:[keyCount objectAtIndex:i]];
            //if the inner dictionary is the updated one, rewrite it with our IOU object
            if([[innerDic objectForKey:@"UUID"] isEqualToString:[iou UUID]])
            {
                updatedInnerDic = @{@"person"       : [iou person],
                                    @"reasonForIOU" : [iou reasonForIOU],
                                    @"amountDue"    : [iou amountDue],
                                    @"date"         : [iou date],
                                    @"UUID"         : [iou UUID]};
                //append updated dictionary to file
                [retrieveIOU setObject:updatedInnerDic forKey:[iou UUID]];
            }
            else
            {
                [retrieveIOU setObject:innerDic forKey:[keyCount objectAtIndex:i]];
            }
        }
        
    }
    //append updated dictionary to file to update IOU's
    [retrieveIOU writeToFile:[self retrieveFilePath] atomically:YES];
}

-(void)deleteIOU : (IOU *) iou
{
    //retrieve contents from file
    NSDictionary *totalIOUs = [NSDictionary dictionaryWithContentsOfFile:[self retrieveFilePath]];
    //create array of keys to use as index
    NSArray *keyCount = [totalIOUs allKeys];
    //append IOU objects to this array
    NSMutableDictionary *retrieveIOU = [[NSMutableDictionary alloc] init];
    //used to represent the inner dictionary
    NSDictionary *innerDic = [[NSDictionary alloc]init];
    
    for(int i = 0; i < [totalIOUs count]; i++)
    {
        //selects inner dictionary from our collection of keys
        if([totalIOUs objectForKey:[keyCount objectAtIndex:i]])
        {
            //retrieve inner dictionary
            innerDic = [totalIOUs objectForKey:[keyCount objectAtIndex:i]];
            //if the inner dictionary is the updated one, rewrite it with our IOU object
            if(![[innerDic objectForKey:@"UUID"] isEqualToString:[iou UUID]])
            {
                [retrieveIOU setObject:innerDic forKey:[keyCount objectAtIndex:i]];
            }
        }
    }
    //append updated dictionary to file to update IOU's
    [retrieveIOU writeToFile:[self retrieveFilePath] atomically:YES];
}

-(NSString *)retrieveFilePath{
    //creates an array that holds all the directories for the application
    NSArray *directories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //retrieves the document folder from our array
    NSString *documents = [directories firstObject];
    //appends the file path onto our directory folder
    NSString *filePath = [documents stringByAppendingPathComponent:@"IOU.txt"];
    return filePath;
}

@end
