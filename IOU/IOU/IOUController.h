//
//  IOUController.h
//  IOU
//
//  Created by Boughdan Gibbons on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IOU.h"

@interface IOUController : NSObject

-(void)addIOU : (IOU *) iou;
-(NSArray *)retrieveIOU;
-(void)updateIOU : (IOU *) iou;
-(void)deleteIOU : (IOU *) iou;

@end
