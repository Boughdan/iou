//
//  IOU.m
//  IOU
//
//  Created by Boughdan Gibbons on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//


#import "IOU.h"


@implementation IOU

@synthesize person;
@synthesize reasonForIOU;
@synthesize amountDue;
@synthesize date;
@synthesize UUID;

@end