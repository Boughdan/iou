//
//  EditViewController.m
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "EditViewController.h"
#import "IOU.h"
#import "IOUController.h"
#import <QuartzCore/QuartzCore.h>
@interface EditViewController ()  <UITextFieldDelegate, UITextInputDelegate, UIAlertViewDelegate, UITextViewDelegate>



@end

@implementation EditViewController

@synthesize currentIOU, ID, personTextField, amountDueTextField, amountPaidTextField, dueDatePicker,
reasonTextField;



-(void)textWillChange:(id<UITextInput>)textInput{

}


-(void)selectionWillChange:(id<UITextInput>)textInput{

}


-(void)textDidChange:(id<UITextInput>)textInput{
    [self resignFirstResponder];
}


-(void)selectionDidChange:(id<UITextInput>)textInput{
    [self resignFirstResponder];
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]){
            [textView resignFirstResponder];
            return NO;
        }
    return YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self.reasonTextField layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.reasonTextField layer] setBorderWidth:2.3];
    [[self.reasonTextField layer] setCornerRadius:15];
    NSLog(@"amount inside the IOU object is %@", currentIOU.amountDue);
    
    personTextField.delegate = self;
    reasonTextField.delegate = self;
    amountPaidTextField.delegate = self;
    amountDueTextField.delegate = self;
    
    [personTextField setText:[currentIOU person]];
    [reasonTextField setText:[currentIOU reasonForIOU]];
    [dueDatePicker setDate:[currentIOU date]];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter stringFromNumber:[currentIOU amountDue]];
    [amountDueTextField setText:[formatter stringFromNumber:[currentIOU amountDue]]];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveChanges:(id)sender {
    IOU *newIOU = [[IOU alloc] init];
    [newIOU setPerson:[personTextField text]];
    [newIOU setReasonForIOU:[reasonTextField text]];
    [newIOU setDate:[dueDatePicker date]];
    [newIOU setUUID:[currentIOU UUID]];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //[formatter stringFromNumber:[currentIOU amountDue]];
    [newIOU setAmountDue:[formatter numberFromString:[amountDueTextField text]]];

    if(amountPaidTextField != nil)
    {
        //get the amount due
        NSNumber *due  = [formatter numberFromString:[amountDueTextField text]];
        double dueDouble = [due doubleValue];
        NSNumber *paid = [formatter numberFromString:[amountPaidTextField text]];
        double paidDouble = [paid doubleValue];
        
        if(dueDouble < 0)
        {
            dueDouble = dueDouble + paidDouble;
        }
        else if(dueDouble > 0)
        {
            dueDouble = dueDouble - paidDouble;
        }
        [newIOU setAmountDue:[NSNumber numberWithDouble:dueDouble]];

    }
    IOUController *iouCon = [[IOUController alloc] init];
    [iouCon updateIOU:newIOU];
}

- (IBAction)editAmountBorrowed:(id)sender {
    [self.amountDueTextField resignFirstResponder];
}

- (IBAction)editPersonBorrowing:(id)sender {
    [self.personTextField resignFirstResponder];
}

- (IBAction)editOwedAmount:(id)sender {
    [self.amountDueTextField resignFirstResponder];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0)
    {
        NSLog(@"cancel");
    }
    else
    {
        NSLog(@"not cancel");
    }
}

- (IBAction)closeIOU:(id)sender {
    IOUController *iouCon = [[IOUController alloc] init];
    [iouCon deleteIOU:currentIOU];
}


- (IBAction)editAmountPaid:(id)sender {
    [amountPaidTextField resignFirstResponder];
}

-(IBAction)editingEnded:(id)sender{
    [sender resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
