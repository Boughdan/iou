//
//  IOUTableViewCell.h
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IOUTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellAmount;
@property (weak, nonatomic) IBOutlet UILabel *cellPerson;
@property (weak, nonatomic) IBOutlet UIButton *cellButton;

@end
