//
//  IOU.h
//  IOU
//
//  Created by Boughdan Gibbons on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IOU : NSObject

@property (strong, atomic) NSString *person;
@property (strong, atomic) NSString *reasonForIOU;
@property (strong, atomic) NSNumber *amountDue;
@property (strong, atomic) NSDate   *date;
@property (strong, atomic) NSString *UUID;
@end