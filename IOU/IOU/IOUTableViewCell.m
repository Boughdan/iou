//
//  IOUTableViewCell.m
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "IOUTableViewCell.h"

@implementation IOUTableViewCell

@synthesize cellPerson, cellAmount, cellButton;

- (void)awakeFromNib {
    // Initialization code
    if(cellAmount == nil)
    {
        NSLog(@"cellAmount = nil");
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
