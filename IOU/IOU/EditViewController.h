//
//  EditViewController.h
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IOU.h"
@interface EditViewController : UIViewController

@property (nonatomic) int ID;
@property (nonatomic) IOU *currentIOU;
@property (weak, nonatomic) IBOutlet UITextField *personTextField;
@property (weak, nonatomic) IBOutlet UITextField *amountDueTextField;
@property (weak, nonatomic) IBOutlet UITextView *reasonTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *dueDatePicker;
@property (weak, nonatomic) IBOutlet UITextField *amountPaidTextField;

@end
