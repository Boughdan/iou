//
//  AddViewController.h
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "IOUController.h"
@interface AddViewController : UIViewController

@end
