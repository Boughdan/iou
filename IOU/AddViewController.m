//
//  AddViewController.m
//  IOU
//
//  Created by Mike Dikkes on 2014-12-12.
//  Copyright (c) 2014 TD Bank Group. All rights reserved.
//

#import "AddViewController.h"

@interface AddViewController () <UITextFieldDelegate, UITextInputDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *person;
@property (weak, nonatomic) IBOutlet UITextField *amountDue;
@property (weak, nonatomic) IBOutlet UITextView *reasonForBorrowing;
@property (weak, nonatomic) IBOutlet UIDatePicker *dateDue;
@property (weak, nonatomic) IBOutlet UISlider *amountSlider;

@end

@implementation AddViewController

@synthesize person;
@synthesize amountDue;
@synthesize reasonForBorrowing;
@synthesize dateDue;
@synthesize amountSlider;



-(void)textWillChange:(id<UITextInput>)textInput{
    
}


-(void)selectionWillChange:(id<UITextInput>)textInput{

}


-(void)textDidChange:(id<UITextInput>)textInput{
      [self resignFirstResponder];
}


-(void)selectionDidChange:(id<UITextInput>)textInput{
    [self resignFirstResponder];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
            if([text isEqualToString:@"\n"]){
                [textView resignFirstResponder];
                return NO;
            }
    return YES;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self.reasonForBorrowing layer] setBorderColor:[[UIColor grayColor] CGColor]];
    [[self.reasonForBorrowing layer] setBorderWidth:2.3];
    [[self.reasonForBorrowing layer] setCornerRadius:15];
    
    self.person.delegate = self;
    self.amountDue.delegate = self;
    self.reasonForBorrowing.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)doneButton{
    if([[person text] isEqualToString:@""] || [[amountDue text] isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"missing fields"
                                                        message:@"You must fill out name and amount"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        IOU *iou = [[IOU alloc] init];
        IOUController *controller = [[IOUController alloc] init];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [iou setPerson:[person text]];
        [iou setAmountDue:[formatter numberFromString:[amountDue text]]];
        [iou setReasonForIOU:[reasonForBorrowing text]];
        [iou setDate:dateDue.date];
        [controller addIOU:iou];
    }
}


- (IBAction)sliderValueChanged:(id)sender {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:2];
    double amnt = (double) amountSlider.value;
    [amountDue setText:[NSString stringWithFormat:@"%.2f",amnt]];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
